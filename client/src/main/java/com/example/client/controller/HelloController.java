package com.example.client.controller;

import com.example.client.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/hello")
public class HelloController {

    @Autowired
    private ConfigService configService;

    @GetMapping(value = "/{firstName}")
    public String sayHello(@PathVariable String firstName) {
        return configService.getHelloMessage() + " " + firstName;
    }
}
