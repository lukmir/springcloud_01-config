package com.example.client.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigService {

    @Value("${hello.message}")
    private String helloMessage;

    public String getHelloMessage() {
        return helloMessage;
    }
}
